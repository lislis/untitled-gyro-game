class Input {
  Serial port;
  String rawValue;
  String[] angles;
  
  Input(Serial p) {
    port = p;
  }
  
  void readRawValue() {
    rawValue = port.readStringUntil('\n');
    extractAngles(rawValue);
  }
  
  void extractAngles(String raws) {
    String trimmed = trim(raws);
    if (trimmed != null) {
      String[] splitted = split(trimmed, ',');
      if (splitted.length == 3) {
         angles = splitted;
      }
    }
  }
  
  String[] getAngles() {
    return angles;
  }
}
