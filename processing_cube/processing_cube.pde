import processing.serial.*;

float x,y,z;
Input theInput;

void setup() {
  size(640,360,P3D);
  z = 0;
  x = 0;
  y = 0;
  
  String portName = Serial.list()[0];
  theInput = new Input(new Serial(this, portName, 9600));
}


void draw() {
  background(0);
  ambientLight(0,200,255);
  
  
  theInput.readRawValue();
  printArray(theInput.getAngles());
  
  if (theInput.getAngles() != null ) {
    
    x = float(theInput.getAngles()[0]);
    z = float(theInput.getAngles()[1]);
    y = float(theInput.getAngles()[2]);
    
    pushMatrix();
    translate(width/2, height/2, 0);
    rotateY(radians(y));
    rotateX(radians(x));
    rotateZ(radians(z));
    //noStroke();
    stroke(255);
    //noFill();
    box(100);
    popMatrix();
  } else {
    textSize(32);
    text("Waiting for Serial input", 50, 50); 
  }
}
